<?php

namespace App\Http\Controllers;

use App\Pago;
use App\Ruta;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $id_ruta = Auth::user()->ruta_id;
        $ruta = Ruta::where('id', $id_ruta)->first();

        $hoy = explode(' ', Carbon::now('America/Jamaica'))[0];

        $pagos = Pago::where('fecha', $hoy)->get();

        $count = 0;

        foreach ($pagos as $pago){
            $count += $pago->valor;
        }

        $perPage = 6;
        $page = Input::get('page');
        $pageName = 'page';
        $page = Paginator::resolveCurrentPage($pageName);
        $offSet = ($page * $perPage) - $perPage;

        $agenda = new Pago();

        $total_pagos = $agenda->where('fecha', $hoy)->offset($offSet)->limit($perPage)->orderByDesc('id')->get();

        $total_registros = Pago::where('fecha', $hoy)->get()->count();

        $posts = new LengthAwarePaginator($total_pagos, $total_registros, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);

        return view('home', ['ruta' => $ruta, 'pagos' => $posts, 'total' =>$count, 'realpagos' => $pagos])->withPosts($posts);
    }
}
