<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Ruta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ClientesController extends Controller
{
    public function index()
    {
        $id_ruta = Auth::user()->ruta_id;
        $ruta = Ruta::where('id', $id_ruta)->first();

        return view('clientes.index', ['ruta' => $ruta]);
    }

    public function save(Request $request)
    {
        $cliente = new Cliente();
        $cliente->nombre = strtoupper($request->nombre);
        $cliente->cedula = $request->cedula;
        $cliente->direccion = $request->direccion;
        $cliente->telefono = $request->telefono;
        $cliente->ruta_id = $request->ruta;
        $cliente->prestamo = str_replace('$', '', str_replace(',', '', $request->prestamo));
        $cliente->dias = $request->dias;
        $cliente->vlr_cuota = str_replace('$', '', str_replace(',', '', $request->cuota));
        $cliente->total_credito = str_replace('$', '', str_replace(',', '', $request->total));
        $cliente->no_cuotas = $request->tcuotas;
        $cliente->cuotas_pagas = 0;
        $cliente->acomulado = 0;
        $cliente->fecha_credito = $request->fecha;
        $cliente->estado = "A";
        $cliente->porcentaje = $request->porcentaje;
        $cliente->save();

        $rutas = Ruta::where('id', Auth::user()->ruta_id)->first();

        $prestamo_total = (double)str_replace('$', '', str_replace(',', '', $request->total));

        $rutas->saldo_cartones = (double)$rutas->saldo_cartones + $prestamo_total;
        $rutas->disponible = $rutas->capital - $rutas->saldo_cartones;
        $rutas->save();

        $ruta = Ruta::where('id', Auth::user()->ruta_id)->first();

        return view('clientes.index', ['ruta' => $ruta, 'success' => 'Cliente Guardado Correctamente.']);
    }

    public function editarIndex()
    {
        $id_ruta = Auth::user()->ruta_id;
        $clientes = Cliente::where('ruta_id', $id_ruta)->get();

        $ruta = Ruta::where('id', $id_ruta)->first();
        return view('clientes.editar_select', ['ruta' => $ruta, 'clientes' => $clientes]);
    }

    public function findByClient()
    {
        $client = strtoupper(trim(Input::get('cliente')));

        $clientes = Cliente::select(
            DB::raw('c.id'),
            DB::raw('c.nombre'),
            DB::raw('c.cedula'),
            DB::raw('c.ruta_id'),
            DB::raw('c.prestamo'),
            DB::raw('c.fecha_credito'),
            DB::raw('c.dias'),
            DB::raw('c.vlr_cuota'),
            DB::raw('c.total_credito'),
            DB::raw('c.no_cuotas'),
            DB::raw('c.cuotas_pagas'),
            DB::raw('c.acomulado'),
            DB::raw('c.ruta_id'),
            DB::raw('rutas.nombre as ruta_nombre')
        )->where('c.cedula', DB::raw("'$client'"))
            ->where('c.estado', 'A')
            ->where('c.ruta_id', Auth::user()->ruta_id)
            ->orWhere('c.nombre', 'LIKE', DB::raw("'%$client%'"))
            ->from('clientes as c')
            ->leftJoin('rutas', 'rutas.id', '=', 'c.ruta_id')
            ->groupBy('c.id')
            ->orderByDesc('c.fecha_credito')
            ->get();


        return view('clientes.editar_select', ['clientes' => $clientes, 'todo' => true]);
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->updateClient($request);

            $id_ruta = Auth::user()->ruta_id;
            $clientes = Cliente::where('ruta_id', $id_ruta)->get();

            $ruta = Ruta::where('id', $id_ruta)->first();
            return view('clientes.editar_select',
                [
                    'ruta' => $ruta,
                    'clientes' => $clientes,
                    'success' => 'Cliente Editado Correctamente!',
                ]);
        }

        if ($request->isMethod('get')) {
            $client = trim(Input::get('cliente'));

            $id_ruta = Auth::user()->ruta_id;
            $cliente = Cliente::where('ruta_id', $id_ruta)
                ->where('id', $client)
                ->first();

            $ruta = Ruta::where('id', $id_ruta)->first();

            return view('clientes.editar', ['ruta' => $ruta, 'cliente' => $cliente]);
        }

    }

    public function updateClient($request)
    {
        $id = $request->id_client;

        $cliente = Cliente::where('id', $id)->first();

        $cliente->nombre = strtoupper($request->nombre);
        $cliente->cedula = $request->cedula;
        $cliente->direccion = $request->direccion;
        $cliente->telefono = $request->telefono;
        $cliente->ruta_id = $request->ruta;
        $cliente->prestamo = str_replace('$', '', str_replace(',', '', $request->prestamo));
        $cliente->dias = $request->dias;
        $cliente->vlr_cuota = str_replace('$', '', str_replace(',', '', $request->cuota));
        $cliente->total_credito = str_replace('$', '', str_replace(',', '', $request->total));
        $cliente->no_cuotas = $request->tcuotas;
        $cliente->porcentaje = $request->porcentaje;
        $cliente->fecha_credito = $request->fecha_credito;
        $cliente->save();
    }
}
