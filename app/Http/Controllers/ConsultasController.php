<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Pago;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ConsultasController extends Controller
{
    public function index()
    {
        $clientes = Cliente::select(
            DB::raw('c.id'),
            DB::raw('c.nombre'),
            DB::raw('c.cedula'),
            DB::raw('c.ruta_id'),
            DB::raw('c.prestamo'),
            DB::raw('c.fecha_credito'),
            DB::raw('c.dias'),
            DB::raw('c.vlr_cuota'),
            DB::raw('c.total_credito'),
            DB::raw('c.no_cuotas'),
            DB::raw('c.cuotas_pagas'),
            DB::raw('c.acomulado'),
            DB::raw('c.ruta_id'),
            DB::raw('rutas.nombre as ruta_nombre')
        )->where('c.estado', "A")
            ->where('c.ruta_id', Auth::user()->ruta_id)
            ->from('clientes as c')
            ->leftJoin('rutas', 'rutas.id', '=', 'c.ruta_id')
            ->groupBy('c.id')
            ->orderBy('c.id')
            ->orderByDesc('c.fecha_credito')
            ->get();

        return view('consultas.estadocuenta', ['clientes' => $clientes]);
    }

    public function findByClient()
    {
        $client = strtoupper(trim(Input::get('cliente')));

        $clientes = Cliente::select(
            DB::raw('c.id'),
            DB::raw('c.nombre'),
            DB::raw('c.cedula'),
            DB::raw('c.ruta_id'),
            DB::raw('c.prestamo'),
            DB::raw('c.fecha_credito'),
            DB::raw('c.dias'),
            DB::raw('c.vlr_cuota'),
            DB::raw('c.total_credito'),
            DB::raw('c.no_cuotas'),
            DB::raw('c.cuotas_pagas'),
            DB::raw('c.acomulado'),
            DB::raw('c.ruta_id'),
            DB::raw('rutas.nombre as ruta_nombre')
        )->where('c.cedula', DB::raw("'$client'"))
            ->where('c.estado', 'A')
            ->where('c.ruta_id', Auth::user()->ruta_id)
            ->orWhere('c.nombre', 'LIKE', DB::raw("'%$client%'"))
            ->from('clientes as c')
            ->leftJoin('rutas', 'rutas.id', '=', 'c.ruta_id')
            ->groupBy('c.id')
            ->orderByDesc('c.fecha_credito')
            ->get();


        return view('consultas.estadocuenta', ['clientes' => $clientes, 'todo' => true]);
    }

    public function historyClient($id)
    {
        $cliente = Cliente::where('id', $id)->first();

        $pagos = Pago::select(
            DB::raw('clientes.id'),
            DB::raw('clientes.ruta_id'),
            DB::raw('clientes.prestamo'),
            DB::raw('clientes.fecha_credito'),
            DB::raw('clientes.dias'),
            DB::raw('clientes.cedula'),
            DB::raw('clientes.vlr_cuota'),
            DB::raw('clientes.total_credito'),
            DB::raw('clientes.no_cuotas'),
            DB::raw('clientes.cuotas_pagas'),
            DB::raw('clientes.acomulado'),
            DB::raw('clientes.ruta_id'),
            DB::raw('rutas.nombre as ruta_nombre'),
            DB::raw('c.valor'),
            DB::raw('c.restante'),
            DB::raw('c.fecha')
        )->where('c.cliente', DB::raw("'$id'"))
            ->where('clientes.estado', 'A')
            ->where('c.ruta_id', Auth::user()->ruta_id)
            ->from('pagos as c')
            ->leftJoin('rutas', 'rutas.id', '=', 'c.ruta_id')
            ->leftJoin('clientes', 'clientes.id', '=', 'c.cliente')
            ->orderByDesc('c.fecha')
            ->get();

        $count = 0;
        foreach ($pagos as $pago){
            $count += (double) $pago->valor;
        }

        return view('consultas.history', ['pagos' => $pagos, 'total' => $count, 'cliente' => $cliente]);
    }
}
