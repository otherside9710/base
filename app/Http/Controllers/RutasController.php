<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Ruta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class RutasController extends Controller
{
    public function index(){
        $rutas = Ruta::all();
        return view('rutas.index', ['rutas' => $rutas]);
    }

    public function save(Request $request){
        $ruta = new Ruta();
        $capital = (double)str_replace('$', '', str_replace(',', '', $request->capital));

        $ruta->nombre = $request->ruta;
        $ruta->capital = (double) $capital;
        $ruta->saldo_cartones = (double) 0;
        $ruta->disponible = (double) $capital;

        $ruta->save();
        Session::put('success', 'Ruta Creada Correctamente.');
        return redirect()->route('ruta');
    }

    public function update(Request $request){
        if ($request->isMethod('post')) {
            $ruta = Ruta::where('id', $request->id)->first();

            $capital = (double)str_replace('$', '', str_replace(',', '', $request->capital));
            $cartones = (double)str_replace('$', '', str_replace(',', '', $request->cartones));
            $disponible = (double)str_replace('$', '', str_replace(',', '', $request->disponible));

            $ruta->nombre = $request->ruta;
            $ruta->capital = (double) $capital;
            $ruta->saldo_cartones = (double) $cartones;
            $ruta->disponible = (double) $disponible;
            $ruta->update();

            Session::put('success', 'Ruta Actualizada Correctamente.');

            return redirect()->route('ruta');
        }

        if ($request->isMethod('get')) {
            $id = trim(Input::get('ruta'));

            $ruta = Ruta::where('id', $id)->first();

            return view('rutas.editar', ['ruta' => $ruta]);
        }
    }
}
