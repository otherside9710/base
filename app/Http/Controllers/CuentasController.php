<?php

namespace App\Http\Controllers;

use App\Ruta;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class CuentasController extends Controller
{
    public function index()
    {
        $user = User::all();
        $rutas = Ruta::all();
        return view('ajustes.conf', ['usuarios' => $user, 'rutas' => $rutas]);
    }

    public function save(Request $request)
    {
        $user = User::where('id', $request->usuario)->first();
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->update();

        Session::put('success', 'Contraseña Actualizada Correctamente.');
        return redirect()->route('ajustes');
    }

    public function createUser(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->estado = "A";
        $user->ruta_id = $request->ruta;
        $user->save();

        Session::put('success', 'Usuario Creado Correctamente.');
        return redirect()->route('ajustes');
    }

}
