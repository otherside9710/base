<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CuentasCanceladasController extends Controller
{
    public function index()
    {
        $clientes = Cliente::select(
            DB::raw('c.id'),
            DB::raw('c.nombre'),
            DB::raw('c.cedula'),
            DB::raw('c.ruta_id'),
            DB::raw('c.prestamo'),
            DB::raw('c.fecha_credito'),
            DB::raw('c.dias'),
            DB::raw('c.vlr_cuota'),
            DB::raw('c.total_credito'),
            DB::raw('c.no_cuotas'),
            DB::raw('c.cuotas_pagas'),
            DB::raw('c.ruta_id'),
            DB::raw('rutas.nombre as ruta_nombre')
        )->where('c.estado', "C")
            ->from('clientes as c')
            ->leftJoin('rutas', 'rutas.id', '=', 'c.ruta_id')
            ->groupBy('c.id')
            ->orderByDesc('c.fecha_credito')
            ->get();

        return view('canceladas.cancel', ['clientes' => $clientes]);
    }

    public function findByClient()
    {
        $client = strtoupper(trim(Input::get('cliente')));

        $clientes = Cliente::select(
            DB::raw('c.id'),
            DB::raw('c.nombre'),
            DB::raw('c.cedula'),
            DB::raw('c.ruta_id'),
            DB::raw('c.prestamo'),
            DB::raw('c.fecha_credito'),
            DB::raw('c.dias'),
            DB::raw('c.vlr_cuota'),
            DB::raw('c.total_credito'),
            DB::raw('c.no_cuotas'),
            DB::raw('c.cuotas_pagas'),
            DB::raw('c.ruta_id'),
            DB::raw('rutas.nombre as ruta_nombre')
        )->where('c.cedula', DB::raw("'$client'"))
            ->where('c.estado', 'C')
            ->orWhere('c.nombre', 'LIKE', DB::raw("'%$client%'"))
            ->from('clientes as c')
            ->leftJoin('rutas', 'rutas.id', '=', 'c.ruta_id')
            ->groupBy('c.id')
            ->orderByDesc('c.fecha_credito')
            ->get();

        return view('canceladas.cancel', ['clientes' => $clientes, 'todo' => true]);
    }
}
