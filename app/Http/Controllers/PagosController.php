<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Pago;
use App\Ruta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PagosController extends Controller
{
    public function index()
    {
        $clientes = Cliente::where('ruta_id', Auth::user()->ruta_id)
            ->where('estado', "A")
            ->get();

        return view('pagos.registrar', ['clientes' => $clientes]);
    }

    public function save(Request $request)
    {
        $valor_pago = (double)str_replace('$', '', str_replace(',', '', $request->valor));

        $nombre_cliente = Cliente::where('id', $request->cliente)->first()->nombre;

        $pago = new Pago();

        $pago->cliente = $request->cliente;
        $pago->cliente_nombre = $nombre_cliente;
        $pago->valor = $valor_pago;
        $pago->fecha = $request->fecha;
        $pago->ruta_id = Auth::user()->ruta_id;
        $pago->save();

        $cliente = Cliente::where('id', $request->cliente)->first();

        $cuota = $cliente->cuotas_pagas;

        if ($valor_pago > 1) {
            $cuota = $cliente->cuotas_pagas + 1;
        }

        $cliente->cuotas_pagas = $cuota;
        $cliente->acomulado = $cliente->acomulado + (double)$valor_pago;
        $cliente->update();

        $pago->restante = $cliente->total_credito - $cliente->acomulado;
        $pago->update();

        $ruta = Ruta::where('id', Auth::user()->ruta_id)->first();
        $ruta->saldo_cartones = $ruta->saldo_cartones - $valor_pago;
        $ruta->disponible = $ruta->disponible + $valor_pago;
        $ruta->update();

        $this->validAccountClient($request->cliente, $valor_pago);


        $clientes = Cliente::where('ruta_id', Auth::user()->ruta_id)
            ->where('estado', "A")
            ->get();
        return view('pagos.registrar', ['clientes' => $clientes, 'success' => 'Pago Guardado Correctamente.']);
    }


    public function validAccountClient($client, $pago)
    {
        $cliente = Cliente::where('id', $client)
            ->where('ruta_id', Auth::user()->ruta_id)->first();

        if ((double)$cliente->no_cuotas == (double)$cliente->cuotas_pagas) {
            $cliente->estado = "C";
            $cliente->update();
            Session::put('success', 'El Cliente Pago toda su deuda, podra verlo en cuentas canceladas.');
        }

        if ((double)$pago >= (double)$cliente->total_credito) {
            $cliente->estado = "C";
            $cliente->update();
            Session::put('success', 'El Cliente Pago toda su deuda, podra verlo en cuentas canceladas.');
        }

        if ($cliente->acomulado + $pago > $cliente->total_credito) {
            $cliente->estado = "C";
            $cliente->update();
            Session::put('success', 'El Cliente Pago todas sus cuotas, podra verlo en cuentas canceladas.');
        }

        $clientes = Cliente::where('ruta_id', Auth::user()->ruta_id)
            ->where('estado', "A")
            ->get();

        return view('pagos.registrar', ['clientes' => $clientes, 'error' => 'El Cliente ya pago todas sus cuotas, mire el estado en cuentas canceladas.']);
    }
}
