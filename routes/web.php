<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $rutas = \App\Ruta::all();
    return view('auth.login', ['rutas' => $rutas]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Clientes
Route::get('clientes', 'ClientesController@index')->name('cliente');
Route::get('cliente/editar/index', 'ClientesController@editarIndex')->name('cliente.editar.index');
Route::match(['get', 'post'], 'cliente/update',
    [
        'as' => 'cliente.update',
        'uses' => 'ClientesController@update'
    ]
);
Route::post('clientes/save', 'ClientesController@save')->name('cliente.save');
Route::get('clientes/findByClient', 'ClientesController@findByClient')->name('cliente.find');
//End Clientes

//Pagos
Route::get('pagos', 'PagosController@index')->name('pago');
Route::post('pagos/save', 'PagosController@save')->name('pago.save');
//End Pagos

//Consultas
Route::get('consultas', 'ConsultasController@index')->name('consulta');
Route::get('consultas/findByClient', 'ConsultasController@findByClient')->name('consulta.client');
Route::get('consultas/historyClient/{id}', 'ConsultasController@historyClient')->name('consulta.history');
//End Consultas

//Cuentas Canceladas
Route::get('canceladas', 'CuentasCanceladasController@index')->name('canceladas');
Route::get('canceladas/findByClient', 'CuentasCanceladasController@findByClient')->name('canceladas.client');
//End Canceladas

//Rutas
Route::get('rutas', 'RutasController@index')->name('ruta');
Route::post('rutas/save', 'RutasController@save')->name('ruta.save');

Route::match(['get', 'post'], 'rutas/update',
    [
        'as' => 'ruta.update',
        'uses' => 'RutasController@update'
    ]
);
//End Rutas

//Ajustes
Route::get('ajustes', 'CuentasController@index')->name('ajustes');
Route::post('ajustes/save', 'CuentasController@save')->name('ajustes.save');
Route::post('ajustes/createUser', 'CuentasController@createUser')->name('user.create');
//End Ajustes
