<nav class="sidebar sidebar-offcanvas menu-eca" id="sidebar">
    <ul class="nav">
        <br>
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="user-wrapper">
                    <div class="profile-image">
                        <img src="{{asset('images/faces/face1.png')}}" alt="profile image">
                    </div>
                    <div class="text-wrapper">
                        <p class="profile-name">{{Illuminate\Support\Facades\Auth::user()->name}}</p>
                        <div>
                            <small class="designation text-muted">Admin</small>
                            <span class="status-indicator online"></span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="nav-item hand">
            <a class="nav-link" href="{{route('home')}}">
                <i class="menu-icon fa fa-chart-line"></i>
                <span class="menu-title">Indicadores</span>
            </a>
        </li>

        <li class="nav-item hand">
            <a class="nav-link" href="{{route('cliente')}}">
                <i class="menu-icon fa fa-user-plus"></i>
                <span class="menu-title">Ingresar Cliente</span>
            </a>
        </li>

        <li class="nav-item hand">
            <a class="nav-link" href="{{route('cliente.editar.index')}}">
                <i class="menu-icon fa fa-pencil-alt"></i>
                <span class="menu-title">Editar Cliente</span>
            </a>
        </li>

        <li class="nav-item hand">
            <a class="nav-link" href="{{route('pago')}}">
                <i class="menu-icon fa fa-cart-plus"></i>
                <span class="menu-title">Registrar Pago</span>
            </a>
        </li>
        <li class="nav-item hand">
            <a class="nav-link" href="{{route('consulta')}}">
                <i class="menu-icon fa fa-address-card"></i>
                <span class="menu-title">Consultar Estado de Cuenta</span>
            </a>
        </li>
        <li class="nav-item hand">
            <a class="nav-link" href="{{route('canceladas')}}">
                <i class="menu-icon fa fa-ban"></i>
                <span class="menu-title">Cuentas Canceladas</span>
            </a>
        </li>
        <li class="nav-item hand">
            <a class="nav-link" href="{{route('ruta')}}">
                <i class="menu-icon fa fa-clipboard"></i>
                <span class="menu-title">Configurar Rutas</span>
            </a>
        </li>
        <li class="nav-item hand">
            <a class="nav-link" href="{{route('ajustes')}}">
                <i class="menu-icon fa fa-wrench"></i>
                <span class="menu-title">Ajuste de la cuenta</span>
            </a>
        </li>
    </ul>
</nav>
