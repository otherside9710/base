@extends('layouts.app')

@section('content')
    @include('__partials.head')
    <style>
        form {
            padding: 0;
        }
    </style>

    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
    @include('__partials.nav')
    <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_sidebar.html -->
        @include('__partials.menu')
        <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-crosshairs-gps text-danger icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right"><br></p>
                                            <div class="fluid-container">
                                                <h4 class="font-weight-medium text-right mb-0">Ruta</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-3 mb-0" style="font-size: 14px;">
                                        <b>{{$ruta->nombre}}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-cash text-warning icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right"><br></p>
                                            <div class="fluid-container">
                                                <h4 class="font-weight-medium text-right mb-0">Capital</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-3 mb-0" style="font-size: 14px;">
                                        <b>{{number_format($ruta->capital, 0, '.', '.')}}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-cash-multiple text-behance icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right"><br></p>
                                            <div class="fluid-container">
                                                <h4 class="font-weight-medium text-right mb-0">Cartones</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-3 mb-0" style="font-size: 14px;">
                                        <b>{{number_format($ruta->saldo_cartones, 0, '.', '.')}}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="mdi mdi-cash-multiple text-success icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="mb-0 text-right"><br></p>
                                            <div class="fluid-container">
                                                <h4 class="font-weight-medium text-right mb-0">Disponible</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-3 mb-0" style="font-size: 14px;">
                                        <b>{{number_format($ruta->disponible, 0, '.', '.')}}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Pendientes-->
                    <div class="row">
                        <div class="col-lg-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        Recaudo Diario / {{explode(' ', \Carbon\Carbon::now('America/Jamaica'))[0]}}
                                    </h4>
                                    <div class="row">
                                        <div class="col-md-8"></div>
                                        <div class="col-md-4">
                                            @if($realpagos != "[]") <label><b>Total
                                                    Recaudo:</b></label> {{number_format($total, 0, '.', '.')}}@endif
                                        </div>
                                    </div>
                                    @if($realpagos != "[]")
                                        <div class="table-responsive" id="table">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Nombre</th>
                                                    <th scope="col">Valor</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($pagos as $pago)
                                                    <tr>
                                                        <th scope="row">{{$pago->id}}</th>
                                                        <td>{{$pago->cliente_nombre}}</td>
                                                        <td>{{number_format($pago->valor, 0, '.', '.')}}</td>
                                                    </tr>
                                                @endforeach
                                                <tr style="background: #0bc4d8; color: white">
                                                    <td><b>TOTAL:</b></td>
                                                    <td></td>
                                                    <td>{{number_format($total, 0, '.', '.')}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <br>
                                        {{ $pagos->appends([])->links() }}
                                    @else
                                        <center><p><b>No Hay Pagos</b></p></center>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--End Pendientes-->

                    <!--Resuletos-->
                    <!--END Resuletos-->
                </div>

                <!-- partial:partials/_footer.html -->
            @include('__partials.footer')
            <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

@endsection


