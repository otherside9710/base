<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Facturacion Web</title>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <style>

        .hand:hover {
            cursor: pointer;
        }

        .login-form-1 {
            padding: 5%;
            box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
        }

        .login-form-1 h3 {
            text-align: center;
            color: #333;
        }

        form {
            padding: 10%;
        }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body class="sidebar-fixed">
@yield('content')
</body>

@include('__partials.scripts')

<script>
    $('#formfix').on('submit', function (e) {
        e.preventDefault();

        $pass1 = $('#pass1').val();
        $pass2 = $('#pass2').val();

        if ($pass1 != $pass2) {
            swal("Contraseña Invalida", "Las contrañas no coinciden.", "error");
            $('#pass1').val("");
            $('#pass2').val("");
        } else {
            $('#formfix').submit();
        }
    });
</script>

<script>
    $('#formuser').on('submit', function (e) {
        e.preventDefault();

        $pass1 = $('#pass11').val();
        $pass2 = $('#pass22').val();

        if ($pass1 != $pass2) {
            swal("Contraseña Invalida", "Las contrañas no coinciden.", "error");
            $('#pass1').val("");
            $('#pass2').val("");
        } else {
            $('#formuser').submit();
        }
    });
</script>


@if(\Illuminate\Support\Facades\Session::has('success'))
    <script>
        swal("Proceso Completado", "{{\Illuminate\Support\Facades\Session::get('success')}}", "success");
    </script>
    <?php
        \Illuminate\Support\Facades\Session::remove('success');
    ?>
@endif

@if(\Illuminate\Support\Facades\Session::has('error'))
    <script>
        swal("Error", "{{\Illuminate\Support\Facades\Session::get('error')}}", "error");
    </script>
    <?php
    \Illuminate\Support\Facades\Session::remove('error');
    ?>
@endif

</html>
