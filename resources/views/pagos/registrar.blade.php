@extends('layouts.app')

@section('content')
    @include('__partials.head')
    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <form action="{{route('pago.save')}}" method="post" style="padding: 3% !important;">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>REGISTRAR PAGO</h4></center>
                                    </div>
                                    @if($clientes != "[]")
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-7">
                                                <label style="font-size: 14px"><b>CLIENTE:</b></label> <label
                                                    id="nombre"></label>
                                            </div>
                                            <div class="col-md-4">
                                                <label style="font-size: 14px"><b>CUOTAS RESTANTES:</b></label> <label
                                                    id="cuota_rest"></label>
                                            </div>
                                        </div>

                                        @if(isset($success))
                                            <div class="alert alert-success" role="alert">
                                                <strong>{{$success}}</strong>
                                            </div>
                                        @endif

                                        @if(isset($error))
                                            <div class="alert alert-danger" role="alert">
                                                <strong>{{$error}}</strong>
                                            </div>
                                        @endif
                                        <br>
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4">
                                                <label>Cliente</label>
                                                <select class="form-control" name="cliente" id="cliente_registrar">
                                                    @foreach($clientes as $cliente)
                                                        <option value="{{$cliente->id}}"
                                                                restantes="{{$cliente->no_cuotas - $cliente->cuotas_pagas}}"
                                                                cuota="{{$cliente->vlr_cuota}}"
                                                                nombre="{{$cliente->nombre}}"
                                                        >{{$cliente->nombre}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Valor</label>
                                                <input class="form-control" value="0" id="valor" name="valor" required>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Fecha</label>
                                                <input class="form-control" type="date" name="fecha" required
                                                       id="fecha">
                                            </div>
                                        </div>
                                        <br>
                                        <center>
                                            <button class="btn btn-secondary" style="margin-top: 10px;" type="submit">
                                                Guardar
                                            </button>
                                        </center>
                                        <br><br>
                                    @else
                                        <br>
                                        <div class="alert alert-info" role="alert">
                                            <strong>No Hay Clientes Para Registrar Pagos.</strong>
                                        </div>
                                        <br>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
    @include('__partials.scripts')

    <script>
        $j = $;

        $j(document).ready(function () {

            $fecha = new Date();
            $year = $fecha.getFullYear();
            $month = $fecha.getMonth() + 1;
            $day = $fecha.getDate();
            $fechaNew = null;

            if ($month.toString().length == 1 && $day.toString().length == 1) {
                $fechaNew = $year + '-' + "0" + $month + '-' + "0" + $day;
                $j('#fecha').val($fechaNew);
                return;
            }

            if ($month.toString().length == 1) {
                $fechaNew = $year + '-' + $month + '-' + "0" + $day;
                $j('#fecha').val($fechaNew);
                return;
            }

            if ($day.toString().length == 1) {
                $fechaNew = $year + '-' + $month + '-' + "0" + $day;
                $j('#fecha').val($fechaNew);
            } else {
                $fechaNew = $year + '-' + $month + '-' + $day;
                $j('#fecha').val($fechaNew);
            }
        });


    </script>
    <script>
        $f = $;
        $f(document).ready(function () {
            $f('#cliente_registrar').trigger('change');
        });

        $f('#cliente_registrar').on('change', function () {
            $cuota_vlr = $f("#cliente_registrar option:selected")[0].attributes.cuota.value;
            $cuota_rest = $f("#cliente_registrar option:selected")[0].attributes.restantes.value;
            $name = $f("#cliente_registrar option:selected")[0].attributes.nombre.value;
            $f('#valor').val($cuota_vlr);
            $f('#cuota_rest').text($cuota_rest);
            $f('#nombre').text($name);
            $f('#valor').trigger('change');
        });

        $('#valor').on('change', function () {
            $val_pago = $(this).val();
            $('#valor').val("$" + numberFormat($val_pago));
        });

        function numberFormat($val) {
            Number.toLocaleString('es-CO');
            $parse = parseFloat($val).toLocaleString(window.document.documentElement.lang);
            return $parse;
        }
    </script>
@endsection
