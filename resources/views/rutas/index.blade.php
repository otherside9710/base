@extends('layouts.app')
<style>
    .table th, .table td {
        padding: 10 !important;
    }

    table tbody tr td, table thead tr th {
        font-size: 11px !important;
        padding-right: 0px !important;
    }
</style>
@section('content')
    @include('__partials.head')
    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12 grid-margin">
                            <div class="card" style="padding: 3%;">
                                <div class="card-body">
                                    <center><h4>CREAR RUTA</h4></center>
                                </div>

                                @if(isset($success))
                                    <div class="alert alert-success" role="alert">
                                        <strong>{{$success}}</strong>
                                    </div>
                                @endif

                                @if(isset($error))
                                    <div class="alert alert-danger" role="alert">
                                        <strong>{{$error}}</strong>
                                    </div>
                                @endif

                                <form action="{{route('ruta.save')}}" method="post" style="padding: 0 !important;">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <label>Nombre</label>
                                            <input name="ruta" required class="form-control">
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Capital</label>
                                            <input name="capital" id="capital" required class="form-control">
                                        </div>

                                        <div class="col-md-2" style="padding-top: 7px">
                                            <br>
                                            <button class="btn btn-outline-success">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 grid-margin">
                            <div class="card" style="padding: 3%;">
                                <div class="card-body">
                                    <center><h4>RUTAS</h4></center>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nombre</th>
                                                <th scope="col">Capital</th>
                                                <th scope="col">Saldo Cartones</th>
                                                <th scope="col">Disponible</th>
                                                <th style="text-align: center" scope="col">Opción</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($rutas as $ruta)
                                                <tr>
                                                    <td>{{$ruta->id}}</td>
                                                    <td>{{$ruta->nombre}}</td>
                                                    <td>{{number_format($ruta->capital, 0, '.', '.')}}</td>
                                                    <td>{{number_format($ruta->saldo_cartones, 0, '.', '.')}}</td>
                                                    <td>{{number_format($ruta->disponible, 0, '.', '.')}}</td>
                                                    <td>
                                                        <form method="get" action="{{route('ruta.update')}}">
                                                            <input type="hidden" value="{{$ruta->id}}" name="ruta">
                                                            <button class="btn btn-outline-success btn-block">Editar</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
    @include('__partials.scripts')

    <script>
        $('#capital').on('change', function () {
            $val_pago = $(this).val().replace(/,/g, '').replace('$', '');;
            $('#capital').val("$" + numberFormat($val_pago));
        });

        function numberFormat($val) {
            Number.toLocaleString('es-CO');
            $parse = parseFloat($val).toLocaleString(window.document.documentElement.lang);
            return $parse;
        }
    </script>

@endsection
