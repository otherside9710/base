@extends('layouts.app')
<style>
    .table th, .table td {
        padding: 10 !important;
    }

    table tbody tr td, table thead tr th {
        font-size: 11px !important;
        padding-right: 0px !important;
    }
</style>
@section('content')
    @include('__partials.head')
    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12 grid-margin">
                            <div class="card" style="padding: 3%;">
                                <div class="card-body">
                                    <center><h4>EDITAR RUTA</h4></center>
                                </div>

                                @if(isset($success))
                                    <div class="alert alert-success" role="alert">
                                        <strong>{{$success}}</strong>
                                    </div>
                                @endif

                                @if(isset($error))
                                    <div class="alert alert-danger" role="alert">
                                        <strong>{{$error}}</strong>
                                    </div>
                                @endif

                                <form action="{{route('ruta.update')}}" method="post" style="padding: 0 !important;">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Nombre</label>
                                            <input type="hidden" value="{{$ruta->id}}" name="id">
                                            <input name="ruta" value="{{$ruta->nombre}}" required class="form-control">
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Capital</label>
                                            <input name="capital"
                                                   id="capital"
                                                   value="${{number_format($ruta->capital, 0, ',', ',')}}"
                                                   required class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Cartones</label>
                                            <input name="cartones"
                                                   id="cartones"
                                                   value="${{number_format($ruta->saldo_cartones, 0, ',', ',')}}"
                                                   required class="form-control">
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Disponible</label>
                                            <input name="disponible"
                                                   id="disponible"
                                                   value="${{number_format($ruta->capital - $ruta->saldo_cartones, 0, ',', ',')}}"
                                                   required class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-3">
                                            <button class="btn btn-block btn-outline-success">Actualizar</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-md-2">
                                        <form action="{{route('ruta')}}" method="get">
                                            <button class="btn btn-outline-info"><i class="fa fa-arrow-left"></i>Volver
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
    @include('__partials.scripts')

    <script>
        $('#capital, #cartones, #disponible').on('change', function () {
            $valor = $(this).val().replace(/,/g, '').replace('$', '');

            $(this).val("$" + numberFormat($valor));
        });

        function numberFormat($val) {
            Number.toLocaleString('es-CO');
            $parse = parseFloat($val).toLocaleString(window.document.documentElement.lang);
            return $parse;
        }
    </script>

@endsection
