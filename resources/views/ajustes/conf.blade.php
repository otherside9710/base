@extends('layouts.app')

@section('content')
    @include('__partials.head')
    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <form action="{{route('ajustes.save')}}" method="post" style="padding: 3% !important;" id="formfix">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>AJUSTES DE LA CUENTA</h4></center>
                                    </div>

                                    @if(isset($success))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{$success}}</strong>
                                        </div>
                                    @endif

                                    @if(isset($error))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{$error}}</strong>
                                        </div>
                                    @endif

                                    <br>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Usuario</label>
                                            <select class="form-control" name="usuario" id="usuario">
                                                @foreach($usuarios as $usuario)
                                                    <option value="{{$usuario->id}}" mail="{{$usuario->email}}">{{$usuario->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" required class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Nueva Contraseña</label>
                                            <input type="password" name="password" id="pass1" required class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Confirmar Contraseña</label>
                                            <input type="password" id="pass2" required class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <center>
                                        <button class="btn btn-secondary" style="margin-top: 10px;" type="submit">
                                            Guardar
                                        </button>
                                    </center>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="{{route('user.create')}}" method="post" style="padding: 3% !important;" id="formuser">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>CREAR USUARIO</h4></center>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <label>Email</label>
                                            <input type="email" name="email" required class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Nombre</label>
                                            <input name="name" required class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <label>Ruta</label>
                                            <select class="form-control" name="ruta">
                                                @foreach($rutas as $ruta)
                                                    <option value="{{$ruta->id}}">{{$ruta->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Nueva Contraseña</label>
                                            <input type="password" name="password" id="pass11" required class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Confirmar Contraseña</label>
                                            <input type="password" id="pass22" required class="form-control">
                                        </div>
                                    </div>
                                    <br>
                                    <center>
                                        <button class="btn btn-secondary" style="margin-top: 10px;" type="submit">
                                            Guardar
                                        </button>
                                    </center>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
    @include('__partials.scripts')
    <script>
        $f = $;
        $f(document).ready(function () {
            $f('#usuario').trigger('change');
        });

        $f('#usuario').on('change', function () {
            $mail = $f("#usuario option:selected")[0].attributes.mail.value;
            $f('#email').val($mail);
        });
    </script>
@endsection
