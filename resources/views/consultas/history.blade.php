@extends('layouts.app')
<style>
    table tbody tr td, table thead tr th {
        font-size: 11px !important;
    }

    form {
        padding: 0 !important;
    }
    .table th, .table td{
        padding: 10 !important;
    }

</style>
@section('content')
    @include('__partials.head')
    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12 grid-margin">
                            <div class="card" style="padding: 3%;">
                                <div class="card-body">
                                    <center><h4>HISTORIAL CLIENTE</h4></center>
                                </div>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4">
                                        <p><b>CLIENTE:</b> {{$cliente->nombre}}</p>
                                    </div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-4">
                                        <p><b>CREDITO:</b> ${{number_format($cliente->total_credito, 0, '.', '.')}}</p>
                                    </div>
                                </div>

                                @if(isset($pagos))
                                    @if($pagos == "[]")
                                        <div class="alert alert-info" role="alert">
                                            <strong>Este cliente no tiene pagos registrados.</strong>
                                        </div>
                                    @endif
                                @endif

                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-2">
                                        <form style="padding: 0" action="{{route('consulta')}}" method="GET">
                                            <button class="btn btn-outline-info" onclick="{{route('consulta')}}">
                                                Volver
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                @if($pagos != "[]")
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Fecha</th>
                                                        <th scope="col">Valor</th>
                                                        <th scope="col">Restante</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($pagos as $pago)
                                                        <tr>
                                                            <td>{{$pago->fecha}}</td>
                                                            <td>${{number_format($pago->valor, 0, '.', '.')}}</td>
                                                            <td>${{number_format($pago->restante, 0, '.', '.')}}</td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td><b>TOTAL ABONADO:</b></td>
                                                        <td><b>${{number_format($total, 0, '.', '.')}}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>VALOR RESTANTE:</b></td>
                                                        <td><b>{{number_format($cliente->total_credito - $cliente->acomulado, 0, '.', '.')}}</b></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
@endsection
