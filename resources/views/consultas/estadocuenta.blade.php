@extends('layouts.app')
<style>
    table tbody tr td, table thead tr th {
        font-size: 11px !important;
        padding-right: 0px !important;
    }

    form {
        padding: 0 !important;
    }

    .table th, .table td {
        padding: 10 !important;
    }


</style>
@section('content')
    @include('__partials.head')
    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12 grid-margin">
                            <div class="card" style="padding: 3%;">
                                <div class="card-body">
                                    <center><h4>ESTADO DE CUENTA DE CLIENTES</h4></center>
                                </div>
                                @if(isset($success))
                                    <div class="alert alert-success" role="alert">
                                        <strong>{{$success}}</strong>
                                    </div>
                                @endif
                                <form action="{{route('consulta.client')}}" method="GET">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Buscar Cliente</label>
                                            <input type="text" required name="cliente" class="form-control"
                                                   placeholder="Cedula o Nombre">
                                        </div>
                                        <div class="col-md-2" style="padding-top: 7px">
                                            <br>
                                            <button class="btn btn-outline-success">Buscar</button>
                                        </div>
                                    </div>
                                </form>

                                @if(isset($todo))
                                    <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-2" style="top:-75px; margin-left: -45px">
                                            <br>
                                            <form action="{{route('consulta')}}" method="GET">
                                                <button class="btn btn-outline-info" onclick="{{route('consulta')}}">
                                                    Volver
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                @endif
                                <br>
                                <br>
                                <div class="table-responsive"
                                     @if(isset($todo)) style="margin-top: -75px !important;" @endif>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Cliente</th>
                                            <th scope="col">Cedula</th>
                                            <th scope="col">Credito</th>
                                            <th scope="col">Fecha</th>
                                            <th scope="col">Cuotas</th>
                                            <th scope="col">Pagos</th>
                                            <th scope="col">C. Restantes</th>
                                            <th scope="col">Vlr Abonado</th>
                                            <th scope="col">Vlr Restante</th>
                                            <th style="text-align: center" scope="col">Opción</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($clientes as $cliente)
                                            @if(1<9)
                                                <tr>
                                                    <td>{{$cliente->id}}</td>
                                                    <td>{{$cliente->nombre}}</td>
                                                    <td>{{$cliente->cedula}}</td>
                                                    <td>{{number_format($cliente->total_credito, 0, '.', '.')}}</td>
                                                    <td>{{$cliente->fecha_credito  }}</td>
                                                    <td>{{$cliente->no_cuotas}}</td>
                                                    <td>{{$cliente->cuotas_pagas}}</td>
                                                    <td>{{$cliente->no_cuotas - $cliente->cuotas_pagas}}</td>
                                                    <td>{{number_format($cliente->acomulado, 0, '.', '.')}}</td>
                                                    <td>{{number_format($cliente->total_credito - $cliente->acomulado, 0, '.', '.')}}</td>
                                                    <td>
                                                        <form method="get"
                                                              action="{{route('consulta.history', $cliente->id)}}">
                                                            <button class="btn btn-outline-success btn-block"><i
                                                                    class="fa fa-eye"></i>Ver
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
@endsection
