@extends('layouts.app')

@section('content')
    @include('__partials.head')

    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <form action="{{route('cliente.update')}}" method="post" style="padding: 3% !important;">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>EDITAR CLIENTE</h4></center>
                                    </div>

                                    @if(isset($success))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{$success}}</strong>
                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Nombres y Apellidos</label>
                                            <input class="form-control" value="{{$cliente->nombre}}" name="nombre"
                                                   required>
                                            <input type="hidden" value="{{$cliente->id}}" name="id_client">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Cedula</label>
                                            <input class="form-control" value="{{$cliente->cedula}}" name="cedula"
                                                   required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Direccion</label>
                                            <input type="text" class="form-control" value="{{$cliente->direccion}}"
                                                   name="direccion">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Telefono</label>
                                            <input type="number" value="{{$cliente->telefono}}" class="form-control"
                                                   name="telefono">
                                        </div>
                                    </div>

                                    <br><br>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>DATOS DEL CREDITO DEL CLIENTE</h4></center>
                                    </div>

                                    <input type="hidden" name="ruta" value="{{$ruta->id}}">

                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-3">
                                            <label>Valor Prestamo</label>
                                            <input class="form-control"
                                                   value="{{number_format($cliente->prestamo, 0, ',', ',')}}" required
                                                   name="prestamo" id="prestamo">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Porcentaje</label>
                                            <select class="form-control" name="porcentaje" id="porcentaje">
                                                <option>{{$cliente->porcentaje}}</option>
                                                <option>15</option>
                                                <option>20</option>
                                                <option>25</option>
                                                <option>30</option>
                                                <option>35</option>
                                                <option>40</option>
                                                <option>45</option>
                                                <option>50</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Dias</label>
                                            <select class="form-control" name="dias" id="dias">
                                                <option>{{$cliente->dias}}</option>
                                                <option>30</option>
                                                <option>45</option>
                                                <option>60</option>
                                                <option>90</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-3">
                                            <label>Fecha Credito</label>
                                            <input class="form-control" type="date" required name="fecha_credito"
                                                   id="fecha_credito" value="{{$cliente->fecha_credito}}">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Cuota Diaria</label>
                                            <input class="form-control" required name="cuota" id="cuota"
                                                   value="{{number_format($cliente->vlr_cuota, 0, ',', ',')}}">
                                        </div>
                                        <div class="col-md-2">
                                            <label>No. Cuotas</label>
                                            <input class="form-control" name="tcuotas" id="tcuotas"
                                                   value="{{number_format($cliente->no_cuotas, 0, ',', ',')}}">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Total Credito</label>
                                            <input class="form-control" name="total" id="total"
                                                   value="{{number_format($cliente->total_credito, 0, ',', ',')}}">
                                        </div>
                                    </div>
                                    <br>
                                    <center>
                                        <button class="btn btn-secondary" style="margin-top: 10px;" type="submit">
                                            Guardar
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form style="padding: 0;" action="{{route('cliente.editar.index')}}">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3">
                                <button class="btn btn-outline-info"><i class="fa fa-arrow-left"></i>Volver</button>
                            </div>
                        </div>
                    </form>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
    @include('__partials.scripts')
    <script>
        $('#prestamo, #dias, #porcentaje').on('change', function () {
            $prestamo = $('#prestamo').val().replace(/,/g, '').replace('$', '');
            $dias = $('#dias').val().replace(/,/g, '').replace('$', '');
            $porcentaje = $('#porcentaje').val().replace(/,/g, '').replace('$', '');

            $base_dias = 30;

            $diario = parseInt($porcentaje) / $base_dias;

            $acomulado = $diario * parseInt($dias);

            $subtotal = (parseInt($prestamo) * parseInt($acomulado)) / 100;

            $total = parseInt($prestamo) + parseInt($subtotal);

            $cuota = $total / parseInt($dias);

            $numCuotas = $total / $cuota;

            $('#tcuotas').val(Math.round($numCuotas));

            $('#total').val("$" + numberFormat($total));
            $('#cuota').val("$" + numberFormat($cuota));
        });

        $('#cuota').on('change', function () {
            $vlr_cuota = $('#cuota').val().replace(/,/g, '').replace('$', '');

            if (parseInt($vlr_cuota) < 4000) {
                $('#cuota').val("");
                $('#cuota').focus();
                return alert("El valor de la cuota no puede ser menor a 4,000");
            }

            $prestamo = $('#prestamo').val().replace('$', '').replace(/,/g, '');
            $dias = $('#dias').val();

            $20p = $prestamo * 0.20;
            $total = parseInt($prestamo) + parseInt($20p);
            $valc = $total / $vlr_cuota;

            $('#tcuotas').val(Math.round($valc));

            $('#cuota').val("$" + numberFormat($(this).val()));
        });

        $('#prestamo').on('change', function () {
            $val = $(this).val().replace(/,/g, '').replace(/\\./g, '').replace('$', '');
            ;
            $(this).val("$" + numberFormat($val));
        });

        function numberFormat($val) {
            Number.toLocaleString('es-CO');
            $parse = parseFloat($val).toLocaleString(window.document.documentElement.lang);
            return $parse;
        }
    </script>
@endsection
