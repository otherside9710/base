@extends('layouts.app')

@section('content')
    @include('__partials.head')

    <div class="container-scroller">
        @include('__partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('__partials.menu')
            <div class="main-panel">
                <div class="content-wrapper">
                    <form action="{{route('cliente.save')}}" method="post" style="padding: 3% !important;">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>DATOS DE CONTACTO DEL CLIENTE</h4></center>
                                    </div>

                                    @if(isset($success))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{$success}}</strong>
                                        </div>
                                    @endif


                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Nombres y Apellidos</label>
                                            <input class="form-control" name="nombre" required>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Cedula</label>
                                            <input class="form-control" name="cedula" required>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <label>Direccion</label>
                                            <input type="text" class="form-control" name="direccion">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Telefono</label>
                                            <input type="number" class="form-control" name="telefono">
                                        </div>
                                    </div>

                                    <br><br>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 grid-margin">
                                <div class="card" style="padding: 3%;">
                                    <div class="card-body">
                                        <center><h4>DATOS DEL CREDITO DEL CLIENTE</h4></center>
                                    </div>

                                    <input type="hidden" name="ruta" value="{{$ruta->id}}">
                                    <input type="hidden" name="fecha" id="fecha">

                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-2">
                                            <label>Prestamo</label>
                                            <input class="form-control" required name="prestamo" id="prestamo">
                                        </div>

                                        <div class="col-md-2">
                                            <label>Forma Pago</label>
                                            <select class="form-control" name="forma_pago" id="fp">
                                                <option value="D">Diario</option>
                                                <option value="S">Semanal</option>
                                                <option value="M">Mensual</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Porcentaje</label>
                                            <select class="form-control" name="porcentaje" id="porcentaje">
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                                <option value="32">32</option>
                                                <option value="33">33</option>
                                                <option value="34">34</option>
                                                <option value="35">35</option>
                                                <option value="36">36</option>
                                                <option value="37">37</option>
                                                <option value="38">38</option>
                                                <option value="39">39</option>
                                                <option value="40">40</option>
                                                <option value="41">41</option>
                                                <option value="42">42</option>
                                                <option value="43">43</option>
                                                <option value="44">44</option>
                                                <option value="45">45</option>
                                                <option value="46">46</option>
                                                <option value="47">47</option>
                                                <option value="48">48</option>
                                                <option value="49">49</option>
                                                <option value="50">50</option>
                                                <option value="51">51</option>
                                                <option value="52">52</option>
                                                <option value="53">53</option>
                                                <option value="54">54</option>
                                                <option value="55">55</option>
                                                <option value="56">56</option>
                                                <option value="57">57</option>
                                                <option value="58">58</option>
                                                <option value="59">59</option>
                                                <option value="60">60</option>
                                                <option value="61">61</option>
                                                <option value="62">62</option>
                                                <option value="63">63</option>
                                                <option value="64">64</option>
                                                <option value="65">65</option>
                                                <option value="66">66</option>
                                                <option value="67">67</option>
                                                <option value="68">68</option>
                                                <option value="69">69</option>
                                                <option value="70">70</option>
                                                <option value="71">71</option>
                                                <option value="72">72</option>
                                                <option value="73">73</option>
                                                <option value="74">74</option>
                                                <option value="75">75</option>
                                                <option value="76">76</option>
                                                <option value="77">77</option>
                                                <option value="78">78</option>
                                                <option value="79">79</option>
                                                <option value="80">80</option>
                                                <option value="81">81</option>
                                                <option value="82">82</option>
                                                <option value="83">83</option>
                                                <option value="84">84</option>
                                                <option value="85">85</option>
                                                <option value="86">86</option>
                                                <option value="87">87</option>
                                                <option value="88">88</option>
                                                <option value="89">89</option>
                                                <option value="90">90</option>
                                                <option value="91">91</option>
                                                <option value="92">92</option>
                                                <option value="93">93</option>
                                                <option value="94">94</option>
                                                <option value="95">95</option>
                                                <option value="96">96</option>
                                                <option value="97">97</option>
                                                <option value="98">98</option>
                                                <option value="99">99</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Plazo Pago</label>
                                            <select class="form-control" name="dias" id="dias">
                                                <option>0</option>
                                                <option>30</option>
                                                <option>45</option>
                                                <option>60</option>
                                                <option>90</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-4">
                                            <label>Cuota</label>
                                            <input class="form-control" required name="cuota" id="cuota">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Total Credito</label>
                                            <input class="form-control" name="total" id="total">
                                        </div>
                                        <div class="col-md-2">
                                            <label>No. Cuotas</label>
                                            <input class="form-control" name="tcuotas" id="tcuotas">
                                        </div>
                                    </div>
                                    <br>
                                    <center>
                                        <button class="btn btn-secondary" style="margin-top: 10px;" type="submit">
                                            Guardar
                                        </button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @include('__partials.footer')
            </div>
        </div>
    </div>
    @include('__partials.scripts')

    <script>
        $j = $;
        $j(document).ready(function () {
            $fecha = new Date();
            $year = $fecha.getFullYear();
            $month = $fecha.getMonth() + 1;
            $day = $fecha.getDate();
            $fechaNew = null;

            if ($month.toString().length == 1 && $day.toString().length == 1) {
                $fechaNew = $year + '-' + "0" + $month + '-' + "0" + $day;
                $j('#fecha').val($fechaNew);
                return;
            }

            if ($month.toString().length == 1) {
                $fechaNew = $year + '-' + $month + '-' + "0" + $day;
                $j('#fecha').val($fechaNew);
                return;
            }

            if ($day.toString().length == 1) {
                $fechaNew = $year + '-' + $month + '-' + "0" + $day;
                $j('#fecha').val($fechaNew);
            } else {
                $fechaNew = $year + '-' + $month + '-' + $day;
                $j('#fecha').val($fechaNew);
            }
        });
    </script>

    <script>
        $('#prestamo, #dias, #porcentaje, #fp').on('change', function () {
            $prestamo = $('#prestamo').val().replace(/,/g, '').replace('$', '');
            $dias = $('#dias').val().replace(/,/g, '').replace('$', '');
            $porcentaje = $('#porcentaje').val().replace(/,/g, '').replace('$', '');
            $fp = $('#fp').val();

            $base_dias = 30;

            $diario = parseInt($porcentaje) / $base_dias;

            $acomulado = $diario * parseInt($dias);

            $subtotal = (parseInt($prestamo) * parseInt($acomulado)) / 100;

            $total = parseInt($prestamo) + parseInt($subtotal);

            //cuota diaria
            $cuota = $total / parseInt($dias);

            if ($fp === "S"){
                $cuota = $cuota * 7;
            }else if ($fp === "M"){
                $cuota = $cuota * 30;
            }

            //numero de cuotas
            $numCuotas = $total / $cuota;

            $('#tcuotas').val(Math.round($numCuotas));

            $('#total').val("$" + numberFormat($total));
            $('#cuota').val("$" + numberFormat($cuota));
        });

        $('#cuota').on('change', function () {
            $vlr_cuota = $('#cuota').val().replace(/,/g, '').replace('$', '');

            if (parseInt($vlr_cuota) < 4000) {
                $('#cuota').val("");
                $('#cuota').focus();
                return alert("El valor de la cuota no puede ser menor a 4,000");
            }

            $prestamo = $('#prestamo').val().replace('$', '').replace(/,/g, '');
            $dias = $('#dias').val();

            $20p = $prestamo * 0.20;
            $total = parseInt($prestamo) + parseInt($20p);
            $valc = $total / $vlr_cuota;

            $('#tcuotas').val(Math.round($valc));

            $('#cuota').val("$" + numberFormat($vlr_cuota));
        });

        $('#prestamo').on('change', function () {
            $val = $(this).val().replace(/,/g, '').replace(/\\./g, '').replace('$', '');
            $(this).val("$" + numberFormat($val));
        });

        function numberFormat($val) {
            Number.toLocaleString('es-CO');
            $parse = parseFloat($val).toLocaleString(window.document.documentElement.lang);
            return $parse;
        }
    </script>
@endsection
